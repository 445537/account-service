# Account Demo Service 

## Overview 

The repository contains an example account service application that provides two REST endpoints to calculate fee/s for transactions provided to the client.

## Requirements

* Java (JDK 11+)
* Git
* Maven (3.6.2+)

## Databases

By default, the application connects to an in-memory **H2** store with predefined test data (see [`V1.0.1__testdata.sql`](/src/main/resources/db/migration/h2/V1.0.1__testdata.sql) script). In order to use the **PostgreSQL** database, configure the datasource in the [`application.properties`](src/main/resources/application.properties) file.

## Common setup

1. Clone the repository `git clone git@gitlab.ics.muni.cz:445537/account-service.git`
2. In the project folder run `mvn install`
3. Run an application `java -jar ./target/account-service-0.0.1.jar`

## Invoke endpoints 

The application has two REST endpoints that allow you to calculate fee/s for the services (transactions) provided to the client. Type one of the following URLs into your browser to execute the fee calculation:

* `http://localhost:8080/clients/{clientId}/fees` - calculates the fee for all transactions of the particular client
* `http://localhost:8080/transactions/{transactionId}/fee` - calculates the fee for the specific transaction
