package com.example.account.strategy;

import com.example.account.entity.CreditCardNetwork;
import com.example.account.entity.Transaction;
import com.example.account.entity.fee.FixedFee;
import com.example.account.enums.FeeType;
import com.example.account.enums.TransactionType;
import com.example.account.repository.FixedFeeRepository;
import com.example.account.strategy.impl.FixedFeeCalculation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class FixedFeeCalculationTest {

    @InjectMocks
    private FixedFeeCalculation fixedFeeCalculation;
    @Mock
    private FixedFeeRepository fixedFeeRepository;

    @Test
    public void shouldCalculateTransactionFee() {
        CreditCardNetwork masterCard = CreditCardNetwork.builder()
                .id(1L)
                .name("MasterCard")
                .build();

        Transaction transaction = Transaction.builder()
                .id(1L)
                .type(TransactionType.DOMESTIC)
                .creditCardNetwork(masterCard)
                .build();

        FixedFee fixedFee = FixedFee.builder()
                .id(1L)
                .feeType(FeeType.FIXED)
                .creditCardNetwork(masterCard)
                .value(BigDecimal.ONE)
                .build();

        Mockito.when(fixedFeeRepository.findByCreditCardNetworkId(masterCard.getId())).thenReturn(Optional.of(fixedFee));
        BigDecimal transactionFee = fixedFeeCalculation.calculate(transaction);

        assertEquals(fixedFee.getValue(), transactionFee);
    }
}
