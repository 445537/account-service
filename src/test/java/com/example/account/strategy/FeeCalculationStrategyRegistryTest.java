package com.example.account.strategy;

import com.example.account.enums.TransactionType;
import com.example.account.repository.FixedFeeRepository;
import com.example.account.strategy.impl.FixedFeeCalculation;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = {
        FeeCalculationStrategyRegistry.class,
        FixedFeeCalculation.class,
})
public class FeeCalculationStrategyRegistryTest {

    @MockBean
    private FixedFeeRepository fixedFeeRepository;
    @Autowired
    private FeeCalculationStrategyRegistry feeCalculationStrategyRegistry;

    @Test
    public void shouldGetStrategyForFixedFee() {
        FeeCalculationStrategy feeCalculationStrategy = feeCalculationStrategyRegistry.getStrategyFor(TransactionType.DOMESTIC);
        assertEquals(feeCalculationStrategy.getClass(), FixedFeeCalculation.class);
    }
}
