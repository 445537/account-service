package com.example.account.service.impl;

import com.example.account.entity.Transaction;
import com.example.account.enums.TransactionType;
import com.example.account.repository.TransactionRepository;
import com.example.account.strategy.FeeCalculationStrategyRegistry;
import com.example.account.strategy.impl.FixedFeeCalculation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class FeeServiceImplTest {

    @InjectMocks
    FeeServiceImpl feeService;

    @Mock
    TransactionRepository transactionRepository;
    @Mock
    FeeCalculationStrategyRegistry feeCalculationStrategyRegistry;
    @Mock
    FixedFeeCalculation fixedFeeCalculation;

    @Test
    public void shouldCalculateClientFixedFees() {
        Long clientId = 1L;
        BigDecimal fee1 = new BigDecimal("0.9900");
        BigDecimal fee2 = new BigDecimal("0.4500");
        BigDecimal fee3 = new BigDecimal("0.1500");

        Transaction t1 = Transaction.builder()
                .id(1L)
                .amount(BigDecimal.ONE)
                .type(TransactionType.DOMESTIC)
                .build();
        Transaction t2 = Transaction.builder()
                .id(2L)
                .amount(BigDecimal.TEN)
                .type(TransactionType.DOMESTIC)
                .build();
        Transaction t3 = Transaction.builder()
                .id(3L)
                .amount(BigDecimal.ZERO)
                .type(TransactionType.DOMESTIC)
                .build();

        when(transactionRepository.findAllByClientId(clientId)).thenReturn(List.of(t1, t2, t3));
        when(feeCalculationStrategyRegistry.getStrategyFor(TransactionType.DOMESTIC)).thenReturn(fixedFeeCalculation);
        when(fixedFeeCalculation.calculate(t1)).thenReturn(fee1);
        when(fixedFeeCalculation.calculate(t2)).thenReturn(fee2);
        when(fixedFeeCalculation.calculate(t3)).thenReturn(fee3);

        BigDecimal overallFee = feeService.calculateClientFees(clientId);

        assertEquals(fee1.add(fee2).add(fee3), overallFee);
        verify(feeCalculationStrategyRegistry, times(3)).getStrategyFor(any());
    }

    @Test
    public void shouldCalculateTransactionFee() {
        BigDecimal fee1 = new BigDecimal("0.9900");

        Transaction t1 = Transaction.builder()
                .id(1L)
                .amount(BigDecimal.ONE)
                .type(TransactionType.DOMESTIC)
                .build();

        when(transactionRepository.findById(t1.getId())).thenReturn(Optional.ofNullable(t1));
        when(feeCalculationStrategyRegistry.getStrategyFor(TransactionType.DOMESTIC)).thenReturn(fixedFeeCalculation);
        when(fixedFeeCalculation.calculate(t1)).thenReturn(fee1);

        BigDecimal transactionFee = feeService.calculateTransactionFee(t1.getId());

        assertEquals(fee1, transactionFee);
        verify(feeCalculationStrategyRegistry, times(1)).getStrategyFor(any());
    }
}
