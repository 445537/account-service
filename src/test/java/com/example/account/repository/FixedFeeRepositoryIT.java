package com.example.account.repository;

import com.example.account.entity.CreditCardNetwork;
import com.example.account.entity.fee.FixedFee;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.math.BigDecimal;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class FixedFeeRepositoryIT {

    @Autowired
    private FixedFeeRepository fixedFeeRepository;
    @Autowired
    private TestEntityManager testEntityManager;

    @Test
    void injectedComponentsAreNotNull(){
        assertThat(fixedFeeRepository).isNotNull();
    }

    @Test
    public void shouldFindByCreditCardNetworkId() {
        CreditCardNetwork maestro = CreditCardNetwork.builder()
                .name("UnionPay")
                .build();
        testEntityManager.persistAndFlush(maestro);
        testEntityManager.detach(maestro);

        FixedFee fixedFee = FixedFee.builder()
                .value(new BigDecimal("0.9900"))
                .creditCardNetwork(maestro)
                .build();
        testEntityManager.persistAndFlush(fixedFee);
        testEntityManager.detach(fixedFee);

        Optional<FixedFee> fee = fixedFeeRepository.findByCreditCardNetworkId(maestro.getId());

        assertThat(fee.isPresent()).isTrue();
        assertThat(fee.get().getCreditCardNetwork().getId()).isEqualTo(maestro.getId());
        assertThat(fee.get().getValue()).isEqualTo(fixedFee.getValue());
    }

}
