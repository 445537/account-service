package com.example.account.repository;

import com.example.account.entity.Client;
import com.example.account.entity.CreditCardNetwork;
import com.example.account.entity.Transaction;
import com.example.account.enums.TransactionType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class TransactionRepositoryIT {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    @Test
    void injectedComponentsAreNotNull(){
        assertThat(transactionRepository).isNotNull();
    }

    @Test
    public void shouldFindAllByClientId() {
        Client client = Client.builder()
                .firstName("John")
                .lastName("Doe")
                .build();
        testEntityManager.persistAndFlush(client);
        testEntityManager.detach(client);

        CreditCardNetwork maestro = CreditCardNetwork.builder()
                .name("Maestro")
                .build();
        testEntityManager.persistAndFlush(maestro);
        testEntityManager.detach(maestro);

        Transaction t1 = Transaction.builder()
                .type(TransactionType.DOMESTIC)
                .amount(new BigDecimal("200.0000"))
                .creditCardNetwork(maestro)
                .client(client)
                .build();
        testEntityManager.persistAndFlush(t1);
        testEntityManager.detach(t1);

        Transaction t2 = Transaction.builder()
                .type(TransactionType.DOMESTIC)
                .amount(new BigDecimal("400.0000"))
                .creditCardNetwork(maestro)
                .client(client)
                .build();
        testEntityManager.persistAndFlush(t2);
        testEntityManager.detach(t2);

        List<Transaction> transactions = transactionRepository.findAllByClientId(client.getId());

        assertThat(transactions.size()).isEqualTo(2);
        assertThat(transactions.stream().anyMatch(tr -> tr.getId().equals(t1.getId()))).isTrue();
        assertThat(transactions.stream().anyMatch(tr -> tr.getId().equals(t2.getId()))).isTrue();
    }

    @Test
    public void shouldAuditCreatedDate() {
        Client client = Client.builder()
                .firstName("John")
                .lastName("Doe")
                .build();
        testEntityManager.persistAndFlush(client);
        testEntityManager.detach(client);

        CreditCardNetwork maestro = CreditCardNetwork.builder()
                .name("Maestro")
                .build();
        testEntityManager.persistAndFlush(maestro);
        testEntityManager.detach(maestro);

        Transaction transaction = Transaction.builder()
                .type(TransactionType.DOMESTIC)
                .amount(new BigDecimal("200.0000"))
                .creditCardNetwork(maestro)
                .client(client)
                .build();
        testEntityManager.persistAndFlush(transaction);
        testEntityManager.detach(transaction);

        Optional<Transaction> result = transactionRepository.findById(transaction.getId());
        assertThat(result).isNotEmpty();
        Transaction resultTransaction = result.get();

        assertThat(resultTransaction.getType()).isEqualTo(transaction.getType());
        assertThat(resultTransaction.getAmount()).isEqualTo(transaction.getAmount());
        assertThat(resultTransaction.getClient().getId()).isEqualTo(client.getId());
        assertThat(resultTransaction.getCreditCardNetwork().getName()).isEqualTo(maestro.getName());
        assertThat(transaction.getCreatedDate()).isNotNull();
    }

}
