package com.example.account.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
public class ClientRestControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void calculateClientFee() throws Exception {
        mockMvc.perform(get("/clients/{clientId}/fees", 1L)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(content().string("2.0400"));
    }
}
