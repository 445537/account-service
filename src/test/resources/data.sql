INSERT INTO client (id, first_name, last_name) VALUES (1, 'John', 'Doe');
INSERT INTO credit_card_network (id, name) VALUES (1, 'MasterCard');
INSERT INTO credit_card_network (id, name) VALUES (2, 'Visa');
INSERT INTO credit_card_network (id, name) VALUES (3, 'Discover');
INSERT INTO credit_card_network (id, name) VALUES (4, 'American Express');
INSERT INTO fixed_fee (id, credit_card_network_id, fee_type, fee_value) VALUES (1, 1, 'FIXED', 0.9900);
INSERT INTO fixed_fee (id, credit_card_network_id, fee_type, fee_value) VALUES (2, 2, 'FIXED', 0.3000);
INSERT INTO fixed_fee (id, credit_card_network_id, fee_type, fee_value) VALUES (3, 3, 'FIXED', 0.5500);
INSERT INTO fixed_fee (id, credit_card_network_id, fee_type, fee_value) VALUES (4, 4, 'FIXED', 0.2000);
INSERT INTO transaction (id, amount, client_id, credit_card_network_id, type, created_date) VALUES (1, 500, 1, 1, 'DOMESTIC', '2022-04-12T12:25:42.00');
INSERT INTO transaction (id, amount, client_id, credit_card_network_id, type, created_date) VALUES (2, 600, 1, 2, 'DOMESTIC', '2022-04-28T16:32:42.00');
INSERT INTO transaction (id, amount, client_id, credit_card_network_id, type, created_date) VALUES (3, 400, 1, 3, 'DOMESTIC', '2022-05-01T06:25:12.00');
INSERT INTO transaction (id, amount, client_id, credit_card_network_id, type, created_date) VALUES (4, 100, 1, 4, 'DOMESTIC', '2022-05-02T09:48:35.00')
