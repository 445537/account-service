package com.example.account.strategy;

import com.example.account.enums.TransactionType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class FeeCalculationStrategyRegistry {

    private final List<FeeCalculationStrategy> strategies;
    private final Map<TransactionType, FeeCalculationStrategy> strategyMap = new HashMap<>();

    @PostConstruct
    private void init() {
        this.strategies.forEach(strategy ->
                strategy.supportedTransactionTypes().forEach(transactionType ->
                        strategyMap.put(transactionType, strategy)
                )
        );
    }

    public FeeCalculationStrategy getStrategyFor(TransactionType transactionType) {
        return Optional.ofNullable(strategyMap.get(transactionType))
                .orElseThrow(() -> new IllegalArgumentException("No strategy found for transaction type: " + transactionType));
    }
}
