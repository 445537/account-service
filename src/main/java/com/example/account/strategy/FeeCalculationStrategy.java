package com.example.account.strategy;

import com.example.account.entity.Transaction;
import com.example.account.enums.TransactionType;

import java.math.BigDecimal;
import java.util.Set;

public interface FeeCalculationStrategy {

    /**
     * Calculates transaction fee by a specific algorithm. Algorithm is selected according to the type of transactions.
     * @param transaction
     * @return calculated fee
     */
    BigDecimal calculate(Transaction transaction);

    /**
     * Returns set of supported transaction types
     * @return set of supported transaction types
     */
    Set<TransactionType> supportedTransactionTypes();
}
