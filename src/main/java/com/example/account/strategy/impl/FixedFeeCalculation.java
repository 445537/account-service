package com.example.account.strategy.impl;

import com.example.account.entity.Transaction;
import com.example.account.entity.fee.FixedFee;
import com.example.account.enums.TransactionType;
import com.example.account.repository.FixedFeeRepository;
import com.example.account.strategy.FeeCalculationStrategy;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.util.Set;


@Component
@RequiredArgsConstructor
@Slf4j
public class FixedFeeCalculation implements FeeCalculationStrategy {

    private final FixedFeeRepository fixedFeeRepository;

    @Override
    public BigDecimal calculate(Transaction transaction) {
        FixedFee fixedFee = fixedFeeRepository.findByCreditCardNetworkId(transaction.getCreditCardNetwork().getId())
                .orElseThrow(() -> {
                    log.error("No fixed fee found for credit card network {}.", transaction.getCreditCardNetwork().getName());
                    return new EntityNotFoundException("Fixed fee for credit card network " + transaction.getCreditCardNetwork().getName() + " has not been found.");
                });
        return fixedFee.getValue();
    }

    @Override
    public Set<TransactionType> supportedTransactionTypes() {
        return Set.of(TransactionType.DOMESTIC);
    }
}
