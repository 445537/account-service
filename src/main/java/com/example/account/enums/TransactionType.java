package com.example.account.enums;

public enum TransactionType {

    DOMESTIC, INTERNATIONAL;
}
