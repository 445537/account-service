package com.example.account.enums;

public enum FeeType {

    FIXED, PERCENTAGE
}
