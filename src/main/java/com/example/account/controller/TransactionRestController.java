package com.example.account.controller;

import com.example.account.service.FeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
@RequestMapping(path = "/transactions")
@RequiredArgsConstructor
public class TransactionRestController {

    private final FeeService feeService;

    @GetMapping(path = "/{transactionId}/fee")
    public ResponseEntity<BigDecimal> calculateFee(@PathVariable Long transactionId) {
        return ResponseEntity.ok(feeService.calculateTransactionFee(transactionId));
    }
}


