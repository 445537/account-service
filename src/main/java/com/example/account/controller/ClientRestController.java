package com.example.account.controller;

import com.example.account.service.FeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
@RequestMapping(path = "/clients")
@RequiredArgsConstructor
public class ClientRestController {

    private final FeeService feeService;

    @GetMapping(path = "/{clientId}/fees")
    public ResponseEntity<BigDecimal> calculateFees(@PathVariable Long clientId) {
        return ResponseEntity.ok(feeService.calculateClientFees(clientId));
    }
}


