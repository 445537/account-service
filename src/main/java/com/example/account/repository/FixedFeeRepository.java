package com.example.account.repository;

import com.example.account.entity.fee.FixedFee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FixedFeeRepository extends JpaRepository<FixedFee, Long> {

    Optional<FixedFee> findByCreditCardNetworkId(Long creditCardNetworkId);
}
