package com.example.account.service.impl;

import com.example.account.entity.Transaction;
import com.example.account.repository.TransactionRepository;
import com.example.account.service.FeeService;
import com.example.account.strategy.FeeCalculationStrategy;
import com.example.account.strategy.FeeCalculationStrategyRegistry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class FeeServiceImpl implements FeeService {

    private final TransactionRepository transactionRepository;
    private final FeeCalculationStrategyRegistry feeCalculationStrategyRegistry;

    @Override
    @Transactional(readOnly = true)
    public BigDecimal calculateClientFees(Long clientId) {
        log.debug("Calculating fees for the client with ID={}", clientId);
        List<Transaction> transactions = transactionRepository.findAllByClientId(clientId);
        BigDecimal resultFee = transactions.stream()
                .map(this::calculateFee)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        log.debug("Resulting sum of the client fees={}", resultFee);
        return resultFee;
    }

    @Override
    @Transactional(readOnly = true)
    public BigDecimal calculateTransactionFee(Long transactionId) {
        log.debug("Calculating fee for the transaction with ID={}", transactionId);
        Transaction transaction = transactionRepository.findById(transactionId)
                .orElseThrow(() -> new EntityNotFoundException("Transaction with ID=" + transactionId + " has not been found."));
        BigDecimal resultFee = calculateFee(transaction);
        log.debug("Resulting transaction fee={}", resultFee);
        return resultFee;
    }

    private BigDecimal calculateFee(Transaction transaction) {
        FeeCalculationStrategy feeCalculationStrategy = feeCalculationStrategyRegistry.getStrategyFor(transaction.getType());
        return  feeCalculationStrategy.calculate(transaction);
    }
}
