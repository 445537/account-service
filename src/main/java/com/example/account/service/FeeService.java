package com.example.account.service;

import java.math.BigDecimal;

public interface FeeService {

    /**
     * Calculates overall fee of the clients transactions. Method returns 0 if no transactions have been found.
     * @param clientId identifier of the client
     * @return overall fee for transactions
     */
    BigDecimal calculateClientFees(Long clientId);


    /**
     * Calculates fee for the single transaction. Method throws an exception if transaction is not found
     * @param transactionId identifier of the transaction
     * @return fee for the transaction
     */
    BigDecimal calculateTransactionFee(Long transactionId);
}
