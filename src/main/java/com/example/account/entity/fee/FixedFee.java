package com.example.account.entity.fee;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.math.BigDecimal;

@Entity
@Data
@SuperBuilder
@NoArgsConstructor
public class FixedFee extends Fee {

    @Column(name = "fee_value", nullable = false, precision = 19, scale = 4)
    private BigDecimal value;

}