CREATE TABLE client
(
    id         BIGINT AUTO_INCREMENT NOT NULL,
    first_name VARCHAR(255)          NOT NULL,
    last_name  VARCHAR(255)          NOT NULL,
    CONSTRAINT pk_client PRIMARY KEY (id)
);

CREATE TABLE credit_card_network
(
    id   BIGINT AUTO_INCREMENT NOT NULL,
    name VARCHAR(255) UNIQUE ,
    CONSTRAINT pk_creditcardnetwork PRIMARY KEY (id)
);

CREATE TABLE fixed_fee
(
    id                     BIGINT AUTO_INCREMENT NOT NULL,
    fee_type               VARCHAR(64),
    credit_card_network_id BIGINT,
    fee_value              DECIMAL(19, 4)        NOT NULL,
    CONSTRAINT pk_fixedfee PRIMARY KEY (id),
    FOREIGN KEY (credit_card_network_id) REFERENCES credit_card_network (id)
);

ALTER TABLE fixed_fee
    ADD CONSTRAINT FK_FIXEDFEE_ON_CREDITCARDNETWORK FOREIGN KEY (credit_card_network_id) REFERENCES credit_card_network (id);

CREATE TABLE transaction
(
    id                     BIGINT AUTO_INCREMENT NOT NULL,
    amount                 DECIMAL(19, 4)        NOT NULL,
    type                   VARCHAR(255),
    client_id              BIGINT                NOT NULL,
    credit_card_network_id BIGINT,
    created_date           TIMESTAMP             NOT NULL,
    CONSTRAINT pk_transaction PRIMARY KEY (id),
    FOREIGN KEY (client_id) REFERENCES client (id),
    FOREIGN KEY (credit_card_network_id) REFERENCES credit_card_network (id)
);
